import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class kwicTest {

    @Test
    void convert() {
        String input = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        String expected = """
                a portrait of the ARTIST as a young man
                the ASCENT of man
                a man is a man but BUBBLESORT is a dog
                DESCENT of man
                a man is a man but bubblesort is a DOG
                descent of MAN
                the ascent of MAN
                the old MAN and the sea
                a portrait of the artist as a young MAN
                a MAN is a man but bubblesort is a dog
                a man is a MAN but bubblesort is a dog
                the OLD man and the sea
                a PORTRAIT of the artist as a young man
                the old man and the SEA
                a portrait of the artist as a YOUNG man
                """;
        assertEquals(expected,new kwic().convert(input));
    }

    @Test
    void getIgnoredWords() {
        String input = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ArrayList<String> output = new ArrayList<>();
        output.add("is");
        output.add("the");
        output.add("of");
        output.add("and");
        output.add("as");
        output.add("a");
        output.add("but");
        assertEquals(output, new kwic().getIgnoredWords(input));
    }

    @Test
    void getTitles() {
        String input = "is\n" +
                "the\n" +
                "of\n" +
                "and\n" +
                "as\n" +
                "a\n" +
                "but\n" +
                "::\n" +
                "Descent of Man\n" +
                "The Ascent of Man\n" +
                "The Old Man and The Sea\n" +
                "A Portrait of The Artist As a Young Man\n" +
                "A Man is a Man but Bubblesort IS A DOG\n";
        ArrayList<String> output = new ArrayList<>();
        output.add("Descent of Man");
        output.add("The Ascent of Man");
        output.add("The Old Man and The Sea");
        output.add("A Portrait of The Artist As a Young Man");
        output.add("A Man is a Man but Bubblesort IS A DOG");
        assertEquals(output, new kwic().getTitles(input));
    }
}

