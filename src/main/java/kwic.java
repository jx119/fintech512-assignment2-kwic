import java.util.*;

public class kwic {
    public String convert(String input) {
        ArrayList<String> ignoredWords = getIgnoredWords(input);
        ArrayList<String> titles = getTitles(input);
        ArrayList<String> temp = new ArrayList<>();
        ArrayList<String> keywords = new ArrayList<>();
        for (String title : titles) {
            String[] wordArr = title.split(" ");

            for (int j = 0; j < wordArr.length; j++) {
                // wordArr[j] = keyword of the title
                if (!ignoredWords.contains(wordArr[j].toLowerCase())) {
                    keywords.add(wordArr[j].toUpperCase());
                    StringBuilder tempStr = new StringBuilder();
                    for (int k = 0; k < wordArr.length; k++) {
                        if (j != k) {
                            tempStr.append(wordArr[k].toLowerCase());
                            tempStr.append(" ");
                        } else {
                            tempStr.append(wordArr[k].toUpperCase());
                            tempStr.append(" ");
                        }
                    }
                    tempStr.deleteCharAt(tempStr.length() - 1); // remove extra space
                    temp.add(tempStr.toString());
                }
            }
        }
        HashMap<String, ArrayList<String>> keyTitlesMap = new HashMap<>();
        for (int i = 0; i < keywords.size(); i++) {
            if (!keyTitlesMap.containsKey(keywords.get(i))) {
                keyTitlesMap.put(keywords.get(i), new ArrayList<>());
            }
            keyTitlesMap.get(keywords.get(i)).add(temp.get(i));
        }
        Map<String, ArrayList<String>> sortedMap = new TreeMap<>(keyTitlesMap);

        ArrayList<String> out = new ArrayList<>();
        for (Map.Entry<String, ArrayList<String>> entry : sortedMap.entrySet()) {
            out.addAll(entry.getValue());
        }
        StringBuilder sb = new StringBuilder();
        for (String eachTitle : out) {
            sb.append(eachTitle);
            sb.append("\n");
        }
        return sb.toString();
    }

    public ArrayList<String> getIgnoredWords(String input) {
        String[] splitArr = input.split("::");
        String ignoredWordsStr = splitArr[0];
        String[] igWordArr = ignoredWordsStr.split("\n");
        ArrayList<String> out = new ArrayList<>(Arrays.asList(igWordArr));
        return out;
    }

    public ArrayList<String> getTitles(String input) {
        String[] splitArr = input.split("::");
        String titlesStr = splitArr[1].substring(1);
        String[] titles = titlesStr.split("\n");
        ArrayList<String> out = new ArrayList<>(Arrays.asList(titles));
        return out;
    }
}
